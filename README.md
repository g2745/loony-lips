# Loony Lips [2021]

Created a Random-Words telling game [Followed Tutorial].

## Playable Example
[Play Loony Lips](https://majorvitec.itch.io/loony-lips)

## Game Overview

### Input Screen

![Input Screen](/images/readme/loony_lips_input_screen.png "Input Screen")

### Result Screen

![Result Screen](/images/readme/loony_lips_result_screen.png "Input Screen")

