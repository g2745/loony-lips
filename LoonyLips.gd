extends Control

var words: Array = []
# var promps: Array = ['a name', 'a movie', 'a opinion', 'a year']
# var story: String = "Once upon a time %s watched %s and thought it was the %s movie of the past %s decades"

var current_story: Dictionary = {}

onready var playerText = $VBoxContainer/HBoxContainer/PlayerText
onready var displayText = $VBoxContainer/DisplayText

func _ready():
	displayText.text = 'Hello and welcome to Loony Lips, we are going to tell a Story!    '
	set_random_story()
	show_question()
	playerText.grab_focus()


func _on_PlayerText_text_entered(text: String):
	print('inserted word: %s' % text)
	update_DisplayText()


func _on_TextureButton_button_down():
	if is_story_done():
		reset_scene()	
	else:
		update_DisplayText()


func update_DisplayText(): 
	if text_not_empty():
		add_words()
		if is_story_done():
			display_story()
		elif not is_story_done():
			clear_display_text()
			show_question()

		clear_player_text()


func get_from_json(filename: String):
	var file: File = File.new()
	var fileFound: int = file.open(filename, File.READ)
	if fileFound == 1:
		print("file not found")
		return []

	var text: String = file.get_as_text()
	var data: String = parse_json(text)
	file.close()

	return data


func set_random_story():
	randomize()
	# var stories = get_from_json("storys.json")
	var stories: Array = $Storys.get_children()
	var randomNumber: int = randi() % stories.size()
	var randomStory: Dictionary = {
		"prompts": stories[randomNumber].prompts,
		"story": stories[randomNumber].story
	}

	current_story = randomStory


func show_question():
	displayText.text += 'May i have ' + current_story.prompts[words.size()] + ' please?'
	

func text_not_empty():
	if playerText.text:
		return true

	return false


func add_words():
	words.append(playerText.text)


func is_story_done():
	return words.size() == current_story.prompts.size()


func display_story():
	displayText.text = current_story.story % words
	end_game()


func end_game():
	playerText.queue_free()
	$VBoxContainer/HBoxContainer/Label.text = 'Again!'


func clear_player_text():
	playerText.clear()

	
func clear_display_text():
	displayText.text = ''


func reset_scene():
	var reloaded: int = get_tree().reload_current_scene()
	print('scene was reloaded: %s' % reloaded)
